# utf8_x_ci

## About

This is a custom collation for MySQL.

## Installation

### Requirements

- MySQL 5.1+

Installing the collation is relatively simple. It doesn't require recompiling anything, however you will require administrative access on your machine.
Adding a new collation does not affect any existing tables; you need to explicitly specify the collation in your create statements and/or queries in order to benefit from it.

### 1. Preparation

1. Find an available collation ID on your MySQL server by following the steps here:
[5.1][id51], [5.5][id55], [5.6][id56].
1. Find the location of MySQL's `Index.xml` file on your system with the command  
`SHOW VARIABLES LIKE 'character_sets_dir'`

[id51]:http://dev.mysql.com/doc/refman/5.1/en/adding-collation-choosing-id.html
[id55]:http://dev.mysql.com/doc/refman/5.5/en/adding-collation-choosing-id.html
[id56]:http://dev.mysql.com/doc/refman/5.6/en/adding-collation-choosing-id.html

### 2. Installation

1. Open the `Index.xml` file in a text editor (you will need to be root).
1. Copy the `<collation ...>` section from the correct `utf8_x_ci-mysql_5.6.xml` file for your version of MySQL.
1. Paste the copied XML into the `<charset name="utf8">...</charset>` section of your `Index.xml` file.
1. Save the file and exit.
1. Restart MySQL server.

### 3. Testing

You can perform the quick test by executing the following query:  
`SELECT * FROM INFORMATION_SCHEMA.COLLATIONS WHERE COLLATION_NAME='utf8_x_ci'`
